# from urllib.parse import urlencode
# mydict = {'z': 'hello', 'q': 'Stanford University, whee!!!', 'a': {'hello': 'world'}}
# print(urlencode(mydict))
# # 'q=Stanford+University%2C+whee%21%21%21'

from urllib import parse as urllib

a = {'a': 'b', 'c': {'d': 'e'}}

url = urllib.urlencode([('%s[%s]'%(k,v.keys()[0]), v.values()[0] ) if type(v)==dict else (k,v) for k,v in a.items()])

url = 'a=b&c%5Bd%5D=e'